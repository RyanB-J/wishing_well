<?php
/*
* newlist.php
* Create your own list to categorize wishes
*/

session_start();
include 'config.php';
include 'functions.php';
?>

<!DOCTYPE html>
<html>
<?php get_meta(); ?>

<body>
	<?php
	$user_id = $_SESSION['user_id'];
	?>
	<header id="header">
		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<a href="index.php"><img src="src/images/logo_flat.png" alt="logo"></a>
			</div>
			<div class="col-sm-8">
				<?php get_nav( $_GLOBAL['main_nav'], 'My Well' ); ?>
			</div>
		</div>
	</header>
	<div class="container-fluid">
		<div class="row">
			<aside id="sidebar" class="col-sm-3 col-md-2 d-none d-sm-block bg-light">
				<h6>List Options</h6>
				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link" href="mywell.php">Go Back</a>
					</li>
				</ul>
			</aside>
			
			<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
				<h3>New List</h3>
				
				<?php
				// Set Post data into variables
				if ( isset( $_POST['title'] ) ) {
					$title = strip_tags( filter_var( trim( $_POST['title'] ), FILTER_SANITIZE_STRING ) );
				}
				if ( isset( $_POST['description'] ) ) {
					$description = strip_tags( filter_var( trim( $_POST['description'] ), FILTER_SANITIZE_STRING ) );
				}
				if ( isset( $_POST['submit'] ) ) {
					$submit = $_POST['submit'];
				}
				
				/********** Before Submit **********/
				if ( ! isset( $submit ) ): 
				?>
				<form action="new-list.php" method="post">
					<h4>Name</h4>
					<div class="form-group">
						<input type="text" class="form-control" name="title" maxlength="255">
						<small class="form-text text-muted">Name of your List</small>
					</div>
					<h4>Description</h4>
					<div class="form-group">
						<textarea class="form-control" rows="5" name="description" maxlength="65535"></textarea>
						<small class="form-text text-muted">Description of your list</small>
					</div>
					<input type="submit" name="submit" class="btn btn-primary">
				</form>
				<?php
				
				/********** After Submit **********/
				else:
				
				/***** Empty Fields *****/
				if ( empty( $title ) ) {
					?>
					<form action="new-list.php" method="post">
						<h4>Name</h4>
						<div class="form-group">
							<input type="text" class="form-control" name="title" maxlength="255" value="<?php echo $title; ?>">
							<?php
							if ( empty( $title ) ) {
								echo '<div class="alert alert-danger">Your list must have a name.</div>';
							}	
							?>
						</div>
						<h4>Description</h4>
						<div class="form-group">
							<textarea class="form-control" rows="5" name="description" maxlength="65535"><?php echo $description; ?></textarea>
							<small class="form-text text-muted">Description of your list</small>
						</div>
						<input type="submit" name="submit" class="btn btn-primary">
					</form>
					<?php
				}
				
				/***** Valid Data *****/
				else {
					
					$db = db_connection();
					
					/* Title already exists */
					$results = $db->query( "SELECT COUNT(*) FROM ww_items WHERE title = '$title' AND is_list = 1" )->fetch();
					if ( $results[0] > 0 ) {
						?>
						<form action="new-list.php" method="post">
							<h4>Name</h4>
							<div class="form-group">
								<input type="text" class="form-control" name="title" maxlength="255" value="<?php echo $title; ?>">
								<?php echo '<div class="alert alert-danger">The name "' . $title . '" already exists.</div>'; ?>
							</div>
							<h4>Description</h4>
							<div class="form-group">
								<textarea class="form-control" rows="5" name="description" maxlength="65535"><?php echo $description; ?></textarea>
								<small class="form-text text-muted">Description of your list</small>
							</div>
							<input type="submit" name="submit" class="btn btn-primary">
						</form>
						<?php
					}
					
					/* Title is unique */
					else {
						
						// Set description to empty if no text was added
						if ( ! isset( $description ) ) {
							$description = "";
						}
						
						// Add the list data to the database
						$db->exec("INSERT INTO ww_items VALUES (NULL, $user_id, '$title', '$description', Null, NULL, 0, NULL, 1)");
						
						// Redirect to mywell
						echo '<script>window.location.replace("mywell.php");</script>';
					}
					$db = null;
				}
				
				endif;
				?>
				
			</main>
		</div>
	</div>
	<?php get_footer(); ?>
</body>
</html>
