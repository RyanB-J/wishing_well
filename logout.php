<?php
/*
* logout.php
* Simple process of logging out
*/
session_start();

// Remove session information
unset( $_SESSION['user_id'] );
session_destroy();
	
// Redirect to home page
echo '<script>window.location.replace("index.php");</script>';

?>