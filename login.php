<?php
/*
* Login.php
* The login/register modal window
*/

session_start();
require_once 'config.php';
require_once 'functions.php';

?>


<script>
$(document).ready( function() {
	$("#open-register-modal").click( function() {
		$('#modal-login').modal('hide');
		$('#modal-register').modal('show');
	});
	$("#open-login-modal").click( function() {
		$('#modal-register').modal('hide');
		$('#modal-login').modal('show');
	});
});
</script>

<?php

// Load Post data into variables and clean them up
if ( isset( $_POST['first'] ) ) {
	$first = strip_tags( filter_var( trim( $_POST['first'] ), FILTER_SANITIZE_STRING ) );
}
if ( isset( $_POST['last'] ) ) {
	$last = strip_tags( filter_var( trim( $_POST['last'] ), FILTER_SANITIZE_STRING ) );
}
if ( isset( $_POST['email'] ) ) {
	$email = strip_tags( filter_var( trim( $_POST['email'] ), FILTER_SANITIZE_STRING ) );
}
if ( isset( $_POST['pass'] ) ) {
	$pass = strip_tags( filter_var( trim( $_POST['pass'] ), FILTER_SANITIZE_STRING ) );
}
if ( isset( $_POST['pass2'] ) ) {
	$pass2 = strip_tags( filter_var( trim( $_POST['pass2'] ), FILTER_SANITIZE_STRING ) );
}
if ( isset( $_POST['form'] ) ) {
	$form = $_POST['form'];
}

// FormGroup
function formGroup ( $output, $name, $label, $type, $value ){

		$output = '<div class="form-group">';
		if ( $label != null ) {
			$output .= '<label for="' . $name . '" class="form-control-label">' . $label . '</label>';
		}
		$output .= '<input type="' . $type . '" class="form-control" name="' . $name . '" ' . ( $value ? 'value="' . $value . '"' : '' ) . '>';
		$output .= '</div>';
		return $output;
}

/*************** Before Submit ***************/

if ( ! isset( $form ) ) {

	// Login Form
	$output = '<form id="login-form" method="post" action="index.php">';
		$output .= formGroup( $output, 'email', 'Email Address', 'email');
		$output .= formGroup( $output, 'pass', 'Password', 'password');
	$output .= '<input type="hidden" name="form" value="login"></form>';
	$output .= '<p>Don\'t have an account? <a href="javascript:undefined" id="open-register-modal">Sign Up</a>!</p>';
	build_modal( 'modal-login', 'Login', $output, 'Login', 'login-form' );
	
	// Register form
	$output = '<form id="register-form" method="post" action="index.php">';
		$output .= formGroup( $output, 'first', 'First Name', 'text');
		$output .= formGroup( $output, 'last', 'Last Name', 'text');
		$output .= formGroup( $output, 'email', 'Email Address', 'email');
		$output .= formGroup( $output, 'pass', 'Password', 'password');
		$output .= formGroup( $output, 'pass2', 'Re-Enter Password', 'password');
	$output .= '<input type="hidden" name="form" value="register"></form>';
	$output .= '<p>Already have an account? <a href="javascript:undefined" id="open-login-modal">Login</a>!</p>';
	build_modal( 'modal-register', 'Sign Up', $output, 'Sign Up', 'register-form' );
}

/*************** After Submit ***************/

/********** Login Form **********/
if ( isset( $form ) and $form == 'login' ) {
	
	/***** Invalid Email *****/
	if ( ! preg_match( '/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/', $email ) ) {
		?>
		<form id="login-form" method="post" action="index.php">
			<div class="form-group">
				<label for="email" class="form-control-label">Email Address</label>
				<input type="text" class="form-control" name="email" value="<?php echo $email ?>">
				<div class="alert alert-danger">Please enter a valid email address.</div>
			</div>
			<div class="form-group">
				<label for="pass" class="form-control-label">Password</label>
				<input type="password" class="form-control" name="pass" value="<?php echo $pass ?>">
			</div>
			<input type="hidden" name="form" value="login">
		</form>
		<p>Don't have an account? <a href="javascript:undefined" id="open-register-modal">Sign Up</a>!</p>
		<?php
	}
	
	/***** Empty Fields *****/
	elseif ( empty( $email ) or empty( $pass ) ) {
		?>
		<form id="login-form" method="post" action="index.php">
			<div class="form-group">
				<label for="email" class="form-control-label">Email Address</label>
				<input type="text" class="form-control" name="email" value="<?php echo $email; ?>">
				<?php
				if ( empty( $email ) ) {
					$output .= '<div class="alert alert-danger">Please enter a valid email address.</div>';
				}
				?>
			</div>
			<div class="form-group">
				<label for="pass" class="form-control-label">Password</label>
				<input type="password" class="form-control" name="pass" value="<?php echo $pass; ?>">
				<?php
				if ( empty( $pass ) ) {
					$output .= '<div class="alert alert-danger">Please enter a valid password.</div>';
				}
				?>
			</div>
			<input type="hidden" name="form" value="login">
		</form>
		<p>Don't have an account? <a href="javascript:undefined" id="open-register-modal">Sign Up</a>!</p>
		<?php
	}
	
	/***** No input errors *****/
	else {
		// Hash password
		$hpass = md5( $pass );
		
		$db = db_connection();
		$result = $db->query( "SELECT COUNT(*) FROM ww_users WHERE email = '$email' AND `password` = '$hpass'" )->fetch();
		
		/***** Correct email and password *****/
		if ( $result[0] > 0 ) {
			
			// Get user_id
			$user_id = $db->query( "SELECT * FROM ww_users WHERE email = '$email'" )->fetch();
			echo $user_id['id'];
			
			// Establish Session
			$_SESSION['user_id'] = $user_id['id'];
			
			// Redirect to mywell
			?>
			<script>
				$('#modal-login').modal('hide');
				window.location.replace("mywell.php");
			</script>
			<?php
		}
		
		/***** Incorrect email or password *****/	
		else {
			?>
			<div class="alert alert-danger">Either your email or your password is incorrect</div>
			<form id="login-form" method="post" action="index.php">
				<div class="form-group">
					<label for="email" class="form-control-label">Email Address</label>
					<input type="text" class="form-control" name="email" value="<?php echo $email; ?>">
				</div>
				<div class="form-group">
					<label for="pass" class="form-control-label">Password</label>
					<input type="password" class="form-control" name="pass" value="<?php echo $pass; ?>">
				</div>
				<input type="hidden" name="form" value="login">
			</form>
			<p>Don't have an account? <a href="javascript:undefined" id="open-register-modal">Sign Up</a>!</p>
			<?php
		}
		$db = null;
	}
}

/********** Register Form **********/
if ( isset( $form ) and $form == 'register' ) {

	/***** Empty Fields *****/
	$field_array = array( $first, $last, $email, $pass, $pass2 );
	$fields_empty = false;
	foreach ( $field_array as $field ) {
		if ( empty( $field ) ) {
			$fields_empty = true;
		}
	}
	if ( $fields_empty == true ) {
		?>
		<form id="register-form" method="post" action="index.php">
			<div class="form-group">
				<label for="first" class="form-control-label">First Name</label>
				<input type="text" class="form-control" name="first" value="<?php echo $first; ?>">
				<?php
				if ( empty ( $first ) ) {
					echo '<div class="alert alert-danger">Please enter your first name.</div>';
				}
				?>
			</div>
			<div class="form-group">
				<label for="last" class="form-control-label">Last Name</label>
				<input type="text" class="form-control" name="last" value="<?php echo $last; ?>">
				<?php
				if ( empty ( $last ) ) {
					echo '<div class="alert alert-danger">Please enter your last name.</div>';
				}
				?>
			</div>
			<div class="form-group">
				<label for="email" class="form-control-label">Email Address</label>
				<input type="text" class="form-control" name="email" value="<?php echo $email; ?>">
				<?php
				if ( empty ( $email ) ) {
					echo '<div class="alert alert-danger">Please enter a valid email address.</div>';
				}
				?>
			</div>
			<div class="form-group">
				<label for="pass" class="form-control-label">Password</label>
				<input type="password" class="form-control" name="pass" value="<?php echo $pass; ?>">
				<?php
				if ( empty ( $pass ) ) {
					echo '<div class="alert alert-danger">Please enter a password.</div>';
				}
				?>
			</div>
			<div class="form-group">
				<label for="pass2" class="form-control-label">Re-Enter Password</label>
				<input type="password" class="form-control" name="pass2" value="<?php echo $pass2; ?>">
				<?php
				if ( empty ( $pass2 ) ) {
					echo '<div class="alert alert-danger">Please re-enter your password.</div>';
				}
				?>
			</div>
			<input type="hidden" name="form" value="register">
		</form>
		<p>Already have an account? <a href="javascript:undefined" id="open-login-modal">Login</a>!</p>
		<?php
	}

	/***** Invalid Email *****/
	elseif ( ! preg_match( '/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/', $email ) ) {
		?>
		<form id="register-form" method="post" action="index.php">
			<div class="form-group">
				<label for="first" class="form-control-label">First Name</label>
				<input type="text" class="form-control" name="first" value="<?php echo $first; ?>">
			</div>
			<div class="form-group">
				<label for="last" class="form-control-label">Last Name</label>
				<input type="text" class="form-control" name="last" value="<?php echo $last; ?>">
			</div>
			<div class="form-group">
				<label for="email" class="form-control-label">Email Address</label>
				<input type="text" class="form-control" name="email" value="<?php echo $email; ?>">
				<div class="alert alert-danger">Please enter a valid email address.</div>
			</div>
			<div class="form-group">
				<label for="pass" class="form-control-label">Password</label>
				<input type="password" class="form-control" name="pass" value="<?php echo $pass; ?>">
			</div>
			<div class="form-group">
				<label for="pass2" class="form-control-label">Re-Enter Password</label>
				<input type="password" class="form-control" name="pass2" value="<?php echo $pass2; ?>">
			</div>
			<input type="hidden" name="form" value="register">
		</form>
		<p>Already have an account? <a href="javascript:undefined" id="open-login-modal">Login</a>!</p>
		<?php
	}
	
	/***** Passwords Match *****/
	elseif ( $pass2 != $pass ) {
		?>
		<form id="register-form" method="post" action="index.php">
			<div class="form-group">
				<label for="first" class="form-control-label">First Name</label>
				<input type="text" class="form-control" name="first" value="<?php echo $first; ?>">
			</div>
			<div class="form-group">
				<label for="last" class="form-control-label">Last Name</label>
				<input type="text" class="form-control" name="last" value="<?php echo $last; ?>">
			</div>
			<div class="form-group">
				<label for="email" class="form-control-label">Email Address</label>
				<input type="text" class="form-control" name="email" value="<?php echo $email; ?>">
			</div>
			<div class="form-group">
				<label for="pass" class="form-control-label">Password</label>
				<input type="password" class="form-control" name="pass" value="<?php echo $pass; ?>">
			</div>
			<div class="form-group">
				<label for="pass2" class="form-control-label">Re-Enter Password</label>
				<input type="password" class="form-control" name="pass2" value="<?php echo $pass2; ?>">
				<div class="alert alert-danger">Your passwords do not match.</div>		
			</div>
			<input type="hidden" name="form" value="register">
		</form>
		<p>Already have an account? <a href="javascript:undefined" id="open-login-modal">Login</a>!</p>
		<?php
	}
	
	/***** No input errors *****/
	else {
		
		$db = db_connection();
		$result = $db->query("SELECT COUNT(*) FROM ww_users WHERE email = '$email'")->fetch();
		
		/* Email aready exists */
		if ( $result[0] > 0 ) {
			?>
				<form id="register-form" method="post" action="index.php">
				<div class="form-group">
					<label for="first" class="form-control-label">First Name</label>
					<input type="text" class="form-control" name="first" value="<?php echo $first; ?>">
				</div>
				<div class="form-group">
					<label for="last" class="form-control-label">Last Name</label>
					<input type="text" class="form-control" name="last" value="<?php echo $last; ?>">
				</div>
				<div class="form-group">
					<label for="email" class="form-control-label">Email Address</label>
					<input type="text" class="form-control" name="email" value="<?php echo $email; ?>">
					<div class="alert alert-danger"><?php echo $email; ?> already exists.
				</div>
				<div class="form-group">
					<label for="pass" class="form-control-label">Password</label>
					<input type="password" class="form-control" name="pass" value="<?php echo $pass; ?>">
				</div>
				<div class="form-group">
					<label for="pass2" class="form-control-label">Re-Enter Password</label>
					<input type="password" class="form-control" name="pass2" value="<?php echo $pass2; ?>">	
				</div>
				<input type="hidden" name="form" value="register">
			</form>
			<p>Already have an account? <a href="javascript:undefined" id="open-login-modal">Login</a>!</p>
			<?php
		}
		
		/* Successful register */
		else {
			// Hash password
			$hpass = md5( $pass );
			
			// Add user
			$result = $db->exec("INSERT INTO ww_users VALUES (NULL, '$first', '$last', '$email', '$hpass')");
			
			// Get user_id
			$user_id = $db->query( "SELECT id FROM ww_users WHERE email = '$email'" )->fetch();
			
			// Establish session
			$_SESSION['user_id'] = $user_id['id'];
			
			// Send data to mywell for first time user
			?>
			<form id="first-time-login" action="mywell.php" method="post">
				<input type="hidden" name="first-time-login" value="true">
			</form>
			<script>
				document.getElementById("first-time-login").submit();
			</script>
			<?php
		}
		
	}
}

?>
