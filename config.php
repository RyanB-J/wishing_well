<?php
/*
* Config.php
* Constants, Globals and Configurations
*/

// Session data
if ( isset( $_SESSION['user_id'] ) ) {
	$login = true;
}

// Database Connections
/*
* Default Server Path for localhost: 127.0.0.1
* Default Port for Apache Server: 3306	
*/
define('DB_SERVER', '127.0.0.1');
define('DB_DATABASE', 'rbainsjordan_dev');
define('DB_PORT', '3306');
define('DB_USER', 'rbainsjordan');
define('DB_PASS', 'bain9500');

// Website Directory Path
/*
* Used for sharing pagees
* Set to the path of the root directory of the site (including root)
*/
define('SITE_PATH', 'http://cs.sierracollege.edu/~rbainsjordan/final_project');

// Upload Directory Path
/*
* Relative path of the uploads directory from site root
*/
define('UPLOAD_PATH', 'uploads/');

// Navigation (If logged in)
if ( $login == true ) {
	$_GLOBAL['main_nav'] = array(
		'Home' => 'index.php',
		'Explore' => 'explore.php',
		'My Well' => 'mywell.php',
		'Logout' => 'logout.php',
	);
}

// Navigation (If not logged in)
else {
	$_GLOBAL['main_nav'] = array(
		'Home' => 'index.php',
		'Explore' => 'explore.php',
		'Login' => 'login',
	);
}
