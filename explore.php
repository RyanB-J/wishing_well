<?php
/*
* explore.php
* View all of the wells created by other people
*/

session_start();
include 'config.php';
include 'functions.php';
?>

<!DOCTYPE html>
<html>
<?php get_meta(); ?>

<body>
	<?php include "login.php" ?>
	<header id="header">
		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<a href="index.php"><img src="src/images/logo_flat.png" alt="logo"></a>
			</div>
			<div class="col-sm-8">
				<?php get_nav( $_GLOBAL['main_nav'], 'Explore' ); ?>
			</div>
		</div>
	</header>
	<div class="container-fluid">
		<div class="row">
			<main role="main" class="col-12 pt-3">
				<div class="container">
					<div class="jumbotron center" id="explore-jumbotron">
						<h1>Explore</h1>
						<p>Here are all of the wells on WishingWell.com. Browse the world and learn exactly what everyone is wishing for.</p>
					</div>
					<div class="card-deck">
						<?php
						// Get all of the users in the database
						$db = db_connection();
						$users = $db->query( "SELECT * FROM ww_users" );
						foreach( $users as $user ) {
							
							$user_id = $user['id'];
							
							// Count All lists from each user
							$lists = $db->query( "SELECT COUNT(*) FROM ww_items WHERE user_id = $user_id AND is_list = 1" )->fetch();
							$lists = $lists[0];
							
							// Count All wishes from each user
							$wishes = $db->query( "SELECT COUNT(*) FROM ww_items WHERE user_id = $user_id AND is_list = 0" )->fetch();
							$wishes = $wishes[0];
							
							// Cut the name if it is too long
							$fullname = $user['first'] . ' ' . $user['last' ];
							if ( strlen( $fullname ) > 20 ) {
								$fullname = substr( $fullname, 0, 20 ) . '...';
							}
							
							// Create a new Well for each user
							new_well( $user_id, $fullname, $lists, $wishes);
							
						}
						
						$db = null;
						?>
					</div>
				</div>
			</main>
		</div>
	</div>
	<?php get_footer(); ?>
</body>
</html>
