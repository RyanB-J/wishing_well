<?php
/*
* Wishing Well
* An online repository for all of your wishes and desires
* Final Project for CSCI59P (Fall 2017)
* Copyright 2017 Ryan Bains-Jordan

* index.php
* Main (Splash) Page for Wishing Well
*/

session_start();
include 'config.php';
include 'functions.php';
?>

<!DOCTYPE html>
<html>
<?php get_meta(); ?>

<body>
	<?php include "login.php"; ?>
	
	<section id="stage">
		<div id="stage-caption">
			<img src="src/images/logo.png" alt="logo">
			<p>Join the ones of people who have created their own personal wishlist with WishingWell.com.</p>
			<?php
			if ( isset( $_SESSION['user_id'] ) ) {
				echo '<a href="mywell.php" class="btn btn-success btn-lg">Go to My Well</a>';
			} else {
				echo '<button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#modal-login">Get Started</button>';
			}
			?>
		</div>
	</section>
	
	<section class="feature-dark">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 feature-caption">
					<h6>Explore the World of Wells</h6>
					<h2>View our online repository of wells created by wishers around the world.</h2>
					<p class="lead">Explore the numerous wells full of wishes made by our community. Learn more about what each person desires and how you can help them obtain it.</p>
					<a href="explore.php" class="btn btn-outline-secondary btn-lg" role="button">Explore</a>
				</div>
				<div class="col-lg-6 text-sm-center">
					<img src="src/images/map.png" alt="World Map">
				</div>
			</div>
		</div>
	</section>
	
	<section id="about">
		<div class="container">
			<div class="row">
				<div class="col-lg-6"></div>
				<div class="col-lg-6 fearure-caption">
					<h6>Become Part of the Fun</h6>
					<p>Becoming a part of WishingWell.com means that you can share your wishes with the world. Create a well, and others will be able to gift you exactly what you want.</p>
					<?php
					if ( ! isset( $_SESSION['user_id'] ) ) {
						echo '<button type="button" class="btn btn-success btn" data-toggle="modal" data-target="#modal-login">Login</button>';
					}
					?>
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>
</body>
	
</html>