<?php
/*
* Functions.php
* Convienence functions and constructors
*/

function db_connection() {
	try {
		// Database Connection with unique port
		$db = new PDO( "mysql:host=".DB_SERVER.";dbname=".DB_DATABASE.";port=".DB_PORT, DB_USER, DB_PASS );
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $db;
		
	} catch ( PDOException $e ) {
		echo '<div class="alert alert-danger">Connection Failed. '. $e->getMessage() .'</div>';
		$db = null;
	}
}

// Header Section
function get_meta( $append ) {
	?>
	<head>
		<meta charset="UTF-8">
		<meta name="description" content="Create your own Wishing Well for sharing your wishes with the world.">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Wishing Well</title>
		<link rel="stylesheet" href="src/bootstrap.css">
		<link rel="stylesheet" href="src/style.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="src/bootstrap.js"></script>
		<?php
		if ( isset( $append ) ) {
			// If it is an array
			if ( is_array( $append ) ) {
				foreach ( $append as $appendage ) {
					echo $appendage;
				}
			} else {
				echo $append;
			}
		}
		?>
	</head>
	<?php
}

// Navigation Menu Constructor
function get_nav( $nav_array, $nav_active ) {
	?>
	<nav id="header-nav">
		<ul class="nav">
			<?php
			if ( ! is_null( $nav_array ) ) {
				foreach ( $nav_array as $nav_name => $nav_href ) {
					
					echo '<li class="nav-item">';
					
					// If nav_href is login
					if ( $nav_href == 'login' ) {
						echo '<a class="nav-link" href="#" data-toggle="modal" data-target="#modal-login">' . $nav_name . '</a>';
					}
					
					// Otherwise
					elseif ( $nav_name == $nav_active ) {
						echo '<a class="nav-link active" href="'. $nav_href .'">' . $nav_name . '<span class="sr-only">(current)</span></a>';
					} else {
						echo '<a class="nav-link" href="'. $nav_href .'">' . $nav_name . '</a>';
					}
					
					echo '</li>';
				}
			}	
			?>
		</ul>
	</nav>
	<?php
}

// Wish Constructor
function new_wish( $link, $image, $title, $rating ) {
	?>
	<a href="<?php echo $link; ?>">
		<div class="card">
			<div class="card-top">
				<img src="<?php echo UPLOAD_PATH . $image; ?>" alt="<?php echo $title; ?>">
			</div>
			<div class="card-block">
				<div class="card-title">
					<h5>
						<?php
							if ( strlen( $title ) > 25 ) {
								$title = substr( $title, 0, 25 ) . '...';
							} 
							echo $title; 
						?>
					</h5>
				</div>
				<?php
				if ( isset($rating) ) {
					echo '<ul class="list-inline">';
					
					if ( $rating >= 0 and $rating <= 5 ) {
						for ( $i = 0; $i < $rating; $i++ ) {
							echo '<li class="list-inline-item star"><img src="src/star-on.svg" alt="star"></li>';
						}
					}
					echo '</ul>';
				}	
					?>
			</div>
		</div>
	</a>
	<?php
}

// Well Constructor
function new_well( $userid, $name, $lists, $wishes ) {
	?>
	<a class="well-card-link" href="well.php?id=<?php echo $userid; ?>">
		<div class="card well-card">
			<div class="well-card-head">
				<div class="card-table">
					<div class="card-table-cell"><?php echo $name; ?></div>
				</div>
			</div>
			<ul class="list-unstyled card-list">
				<li>Lists: <?php echo $lists; ?></li>
				<li>Wishes: <?php echo $wishes; ?></li>
			</ul>
		</div>
	</a>
	<?php
}

// Function to build a bootstrap modal
function build_modal( $id, $title, $body, $button, $form_id ) {
	?>
	<div class="modal fade" id="<?php echo $id; ?>">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"><?php echo $title; ?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<?php if ( $body ) { echo $body; } ?>
				</div>
				<div class="modal-footer">
					<?php
					if ( strtolower($button) == 'close' ) {
						echo '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>';
					} else {
						echo '<button type="button" class="btn btn-primary" id="' . $id . '-submit">' . $button . '</button>';
					}
					?>
				</div>
			</div>
		</div>
	</div>
	<?php
	if ( ! is_null( $form_id ) ) {
	?>
	<script>
		$(document).ready( function() {
			$("#<?php echo $id; ?>-submit").click( function(e) {
				e.preventDefault();
				$.ajax({
					type: 'POST',
					url: 'login.php',
					data: $('#<?php echo $form_id; ?>').serialize(),
					success: function(msg) {
						$('#<?php echo $id; ?> .modal-body').html(msg);
					}
				});
			});
		});
	</script>
	<?php
	}
}

// Create Share Modal
function share_modal( $type, $id ) {
	$url = SITE_PATH . '/' . $type . '.php?id=' . $id;
	$share = '
		<div class="form-group">
			<label for="link">Link</label>
			<input type="text" id="link" class="form-control" value="' . $url . '" readonly>
			<small class="form-text text-muted">Give this link to someone you wish to share '. ($type == 'well' ? 'this well' : 'this wish') .' with.</small>
		</div>
		<div class="center">
			<input type="button" class="btn btn-primary" onclick="copyToClipboard()" value="Copy to Clipboard">
		</div>';
	$share .= '<script>function copyToClipboard() {
		var copyText = document.getElementById("link");
		copyText.select();
		document.execCommand("Copy");
		}</script>';
		
	build_modal( 'modal-share', 'Share ' . ucfirst($type), $share, 'close', null );
}

// Fix user entered links
function validate_link( $link, $protocall = true  ) {
	$link = strip_tags( trim( $link ) );
	
	// If protocall was included
	if ( preg_match( '/(^\w+:|^)\/\//', $link ) ) {
		
		if ( $protocall == true ) {
			return $link;
		} else {
			$link = preg_replace('/(^\w+:|^)\/\//', '', $link);
			return $link;
		}
	} 
	
	// If protocall was not included
	else {
		
		if ( $protocall == true ) {
			return 'http://' . $link;
		} else {
			return $link;
		}
	}
}

// Footer Section
function get_footer() {
	?>
	<footer id="footer">
		<div class="container">
			<div class="row">
				<h6 class="m-auto">CSCI59P - Ryan Bains-Jordan</h6>	
			</div>
		</div>
	</footer>
	<?php
}



?>