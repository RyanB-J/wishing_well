<?php
/*
* newwish.php
* Create your own wish
*/

session_start();
include 'config.php';
include 'functions.php';
?>

<!DOCTYPE html>
<html>

<?php get_meta( '<link rel="stylesheet" href="src/stars.css">' ); ?>
<body>

<script>
$(function() {
	// Disable enter key on all except description box
	$(document).keypress(
    function(event){
    	focused = document.querySelector(":focus");
     	if (event.which == '13' && focused.name != 'description') {
        	event.preventDefault();
      	}
	});

	// We can attach the `fileselect` event to all file inputs on the page
	$(document).on('change', ':file', function() {
		var input = $(this),
		numFiles = input.get(0).files ? input.get(0).files.length : 1,
		label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [numFiles, label]);
	});

	// We can watch for our custom `fileselect` event like this
	$(document).ready( function() {
		$(':file').on('fileselect', function(event, numFiles, label) {

			var input = $(this).parents('.input-group').find(':text'),
			log = numFiles > 1 ? numFiles + ' files selected' : label;

			if( input.length ) {
				input.val(log);
			} else {
				if( log ) alert(log);
			}
		});
	});
});

// Hide uploader until Choose File is clicked
$(document).ready( function() {
	$('#fileUploader').hide();
	$('#fileToUpload').click( function() {
		$('#fileUploader').show(500);
	});
});

</script>

<?php
//  ************		C H E C K  F O R  V A L I D  U S E R  			************
	if (! isset($_SESSION["user_id"])){
			echo '<script>';
			echo 'window.location.replace("index.php");';
			echo '</script>';
	}
?>

<?php
	//  ************		G E T  D A T A  T O  E D I T  A  W I S H  		************
	if ($_SERVER["REQUEST_METHOD"] == "GET") {

		$itm_user = $_SESSION["user_id"];
		$wish_id = $_GET["id"];
		$_SESSION["wish_id"] = $wish_id;

		$db = db_connection();
		$wish = $db->query("SELECT * FROM `ww_items` WHERE user_id = $itm_user AND id = $wish_id" )->fetch();
		$_SESSION["image"] = $wish['image'];
	}
?>

<?php
//  ************	P O S T  S E C T I O N  -  U P L O A D  I M A G E  	************

	if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] == "Upload Image") {
		// Constants
		//define("TARGET_DIR", "uploads/");
		define("MAXIMG_SIZ", "5000000");
		define("MAXIMGWIDTH", "600");

		// Define variables and set to empty values
		$errMsg = $name = $success = "";

		$target_file = UPLOAD_PATH . basename($_FILES["fileToUpload"]["name"]);
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

		//  Test for Empty Values & Clean Posted Variable
		$name = test_input($_FILES["fileToUpload"]["name"]);
		if(empty($name)) {
			$errMsg = "A File is Required";
			$uploadOk = 0;
		}

		// Check If an Actual Image or Fake
		if(isset($_POST["submit"]) && $uploadOk == 1) {
		    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
		    if($check !== false) {
		        $uploadOk = 1;
		    } else {
		        $errMsg = "File is not an image.";
		        $uploadOk = 0;
		    }
		}

		// Check file size
		if (($_FILES["fileToUpload"]["size"] > MAXIMG_SIZ) && $uploadOk == 1) {
		    $errMsg = "Sorry, your file is too large.";
		    $uploadOk = 0;
		}

		// Allow certain file formats
		if( ( $imageFileType != "jpg" && $imageFileType != "jpeg" ) && $uploadOk == 1 ){
		    $errMsg = "Sorry, only JPG & JPEG files are allowed.";
		    $uploadOk = 0;
		}

		// If Checks Passed, Try to Upload the File
		if ($uploadOk == 1) {
		    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
		        $success = "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
		    } else {
		        $errMsg = "Sorry, There was a Problem Uploading Your File.";
		    }
		}

		// Resize the Image
		resizeImage($target_file, MAXIMGWIDTH);

		// Store the Image Name & Path in Memory
		$_SESSION["image"] = basename($_FILES["fileToUpload"]["name"]);

	}

	function test_input($data) {
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  return $data;
	}

    function resizeImage($imagePath, $width) {

        $input_image = $imagePath;                                          // Set the path to the image to resize
        $size = getimagesize( $input_image );                               // Get the size of the original image into an array
        $thumb_width = $width;                                              // Set the new width of the image
        $thumb_height = ( int )(( $thumb_width/$size[0] )*$size[1] );       // Calculate the height with same aspect ratio
        $thumbnail = ImageCreateTrueColor( $thumb_width, $thumb_height );   // Create a new true color image in the memory
        $src_img = ImageCreateFromJPEG( $input_image );                     // Create a new image from file 

        ImageCopyResampled( $thumbnail, $src_img, 0, 0, 0, 0, $thumb_width, $thumb_height, $size[0], $size[1] );  // Create the resized image
        
        ImageJPEG( $thumbnail, $imagePath );                                // Save the image as resized.jpg
        ImageDestroy( $thumbnail );                                         // Clear the memory of the tempory image 

    }
?>


<?php
//  ************	P O S T  S E C T I O N  -  U P D A T E  A  W I S H  	************
	if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] == "Update Your Wish!") {

		$itm_user = $_SESSION["user_id"];
		$itm_wish = $_SESSION["wish_id"];
		$itm_imag = $_SESSION["image"];
		$itm_name = strip_tags( filter_var( trim( $_POST["wish_name"] ), FILTER_SANITIZE_STRING ) );
		$itm_desc = strip_tags( filter_var( trim( $_POST["description"] ), FILTER_SANITIZE_STRING ) );;
		$itm_list = $_POST["list"];
		$itm_link = validate_link( strip_tags( filter_var( trim( $_POST["link"] ), FILTER_SANITIZE_STRING ) ), true );
		$itm_rate = $_POST["rating"];

		$db = db_connection();
		
		$db->exec("UPDATE ww_items SET title = '$itm_name', description = '$itm_desc', list = '$itm_list', link = '$itm_link', rating = '$itm_rate', image = '$itm_imag' WHERE id = '$itm_wish' AND user_id = '$itm_user'");

		// Cleanup Session Information
		unset($_SESSION["image"]);
		unset($_SESSION["wish_id"]);

		echo '<script>window.location.replace("mywell.php");</script>';

	}
?>

<?php
//  ************	P O S T  S E C T I O N  -  D E L E T E  A  W I S H  	************
	if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] == "Delete Your Wish!") {

		$itm_user = $_SESSION["user_id"];
		$itm_wish = $_SESSION["wish_id"];
		
		$db = db_connection();
		
		$db->exec("DELETE FROM ww_items WHERE id = '$itm_wish' AND user_id = '$itm_user'");

		// Cleanup Session Information
		unset($_SESSION["image"]);
		unset($_SESSION["wish_id"]);

		echo '<script>window.location.replace("mywell.php");</script>';

	}
?>

<?php
	function compare_rating( $num, $post_rating ){
		if ($num == $post_rating && !empty($post_rating)){
			return '<input id="rating' . $num . '" type="radio" name="rating" value="' . $num . '"" checked >';
		}else{
			return '<input id="rating' . $num . '" type="radio" name="rating" value="' . $num . '">';
		}

	}
?>

<!--  ************				F O R M  S E C T I O N 			 ************ -->
	<header id="header">
		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<a href="index.php"><img src="src/images/logo_flat.png" alt="logo"></a>
			</div>
			<div class="col-sm-8">
				<?php get_nav( $_GLOBAL['main_nav'], 'My Well' ); ?>
			</div>
		</div>
	</header>
	<div class="container-fluid">
		<div class="row">
			<aside id="sidebar" class="col-sm-3 col-md-2 d-none d-sm-block bg-light">
				<h6>Wish Options</h6>
				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link" href="#">Share Wish</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="mywell.php">Go Back</a>
					</li>
				</ul>
			</aside>
			
			<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
				<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
				<h3>Edit Wish</h3>
				<div class="row">
					<div class="col-sm-6">
						<div class="center">
						<?php
						if ( ( $_SERVER["REQUEST_METHOD"] == "POST" ) && ! empty( $target_file ) ) {
							echo '<img src="' . $target_file . '" class="wish-img" alt="picture">';
						}elseif (!empty($wish['image'])) {
							echo '<img src="' . UPLOAD_PATH . $wish['image'] . '" class="wish-img" alt="picture">';
						}else{
							echo '<img src="src/images/picture.png" class="wish-img" alt="picture">';
						}
						?>
						</div>
						<div id="container">
							<div class="form-upload">
							    <br>
							    <div class="input-group">
									<label class="input-group-btn file-browse" for="fileToUpload">
										<span class="btn btn-primary">Browse&hellip;
											<input type="file" class="d-none" name="fileToUpload" id="fileToUpload" accept='image/jpg,image/jpeg'>
										</span>
									</label>
									<input type="text" class="form-control" readonly>
							    </div>
								<?php 
								if ( ! empty( $errMsg ) ) {
									echo '<div class="alert alert-warning error">' . $errMsg . '</div>';
								}
								?>
								<hr>
								<input type="submit" class="btn btn-primary" value="Upload Image" id="fileUploader" name="submit">
							</div>

							<div class="form-upload">
								<div class='col'>
								<?php
									if (($_SERVER["REQUEST_METHOD"] == "POST") && !empty($success)){
										echo $success;
									}
								?>
								</div>
							</div>
						</div>


					</div>
					<div class="col-sm-6">
							<h4>Name</h4>
							<div class="form-group">
							<?php  // Re-populate Form Field Values after POST
								if (!empty($_POST["wish_name"])){
									echo '<input type="text" class="form-control" name="wish_name" value="' . $_POST["wish_name"] . '">';
								}else{
									echo '<input type="text" class="form-control" name="wish_name" value="' . $wish['title'] . '">';
								}
							?>
								<small class="form-text text-muted">Name of your wish</small>
							</div>
							<h4>Description</h4>
							<div class="form-group">
								<?php  // Re-populate Form Field Values after POST
									if (!empty($_POST["description"])){
										echo '<textarea class="form-control" rows="5" name="description">' . $_POST["description"] . '</textarea>';
									}else{
										echo '<textarea class="form-control" rows="5" name="description">' . $wish["description"] . '</textarea>';
									}
								?>								
								<small class="form-text text-muted">Description of your wish</small>
							</div>
							<h4>Link</h4>
							<div class="form-group">
								<?php  // Re-populate Form Field Values after POST
									if (!empty($_POST["link"])){
										echo '<input type="text" class="form-control" name="link" value="' . $_POST["link"] . '">';
									}else{
										echo '<input type="text" class="form-control" name="link" value="' . $wish["link"] . '">';
									}
								?>
								<small class="form-text text-muted">A link to your wish. Enter the URL of the site</small>
							</div>
							<h4>List</h4>
							<div class="form-group">
								<select class="form-control" name="list">
									<?php
									// Unlisted List for any wishes without a list
									if ( $_POST['list'] == 0 ) {
										echo '<option selected>Unlisted</option>';
									} else {
										echo '<option>Unlisted</option>';
									}
										
									// List all of the user created lists
									$db = db_connection();
									$user_id = $_SESSION['user_id'];
									$wish_id = $_GET['id'];
									$results = $db->query( "SELECT list FROM ww_items WHERE id = $wish_id" )->fetch();
									$list_id = $results['list'];
									$results = $db->query( "SELECT id,title FROM ww_items WHERE is_list = 1 AND user_id = $user_id" );
									foreach ( $results as $row ) {
										if ( $row['id'] == $list_id or $row['id'] == $_POST['list'] ) {
											echo '<option value="' . $row['id'] . '" selected>' . $row['title'] . '</option>';
										} else {
											echo '<option value="' . $row['id'] . '">' . $row['title'] . '</option>';
										}
									}
									$db = null;
									?>
								</select>
								<small class="form-text text-muted">The List that this wish is tied to</small>
							</div>
							<h4>Rating</h4>

							<span class="starRating">
								<?php 
								if(!empty($_POST["rating"])){
									echo compare_rating( 5, $_POST["rating"] ); 
								}else{
									echo compare_rating( 5, $wish["rating"] ); 
								} ?>
								<label for="rating5">5</label>

								<?php 
								if(!empty($_POST["rating"])){
									echo compare_rating( 4, $_POST["rating"] ); 
								}else{
									echo compare_rating( 4, $wish["rating"] ); 
								} ?>								
								<label for="rating4">4</label>

								<?php 
								if(!empty($_POST["rating"])){
									echo compare_rating( 3, $_POST["rating"] ); 
								}else{
									echo compare_rating( 3, $wish["rating"] ); 
								} ?>	
								<label for="rating3">3</label>

								<?php 
								if(!empty($_POST["rating"])){
									echo compare_rating( 2, $_POST["rating"] ); 
								}else{
									echo compare_rating( 2, $wish["rating"] ); 
								} ?>	
								<label for="rating2">2</label>

								<?php 
								if(!empty($_POST["rating"])){
									echo compare_rating( 1, $_POST["rating"] ); 
								}else{
									echo compare_rating( 1, $wish["rating"] ); 
								} ?>	
								<label for="rating1">1</label>
							</span>



							<small class="form-text text-muted">How much do you want it?</small>
							<br>
							<div>
								<input type="submit" value="Update Your Wish!" class="btn btn-primary float-left" name="submit">
								<input type="submit" value="Delete Your Wish!" class="btn btn-danger float-right" name="submit">
							</div>
					</div>
				</div>
			</form>
			</main>
		</div>
	</div>
	<?php get_footer(); ?>
</body>
</html>
