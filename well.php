<?php
/*
* well.php
* Someone else's well
*/

session_start();
include 'config.php';
include 'functions.php';
?>

<!DOCTYPE html>
<html>
<?php get_meta(); ?>

<body>
	<?php
	include "login.php";
	
	// Get the correct user id from whomever's well was clicked
	$user_id = $_GET['id'];
	
	// Build the share modal
	share_modal( 'well', $user_id );
	?>
	<header id="header">
		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<a href="index.php"><img src="src/images/logo_flat.png" alt="logo"></a>
			</div>
			<div class="col-sm-8">
				<?php get_nav( $_GLOBAL['main_nav'], 'Explore' ); ?>
			</div>
		</div>
	</header>
	<div class="container-fluid">
		<div class="row">
			<aside id="sidebar" class="col-sm-3 col-md-2 d-none d-sm-block bg-light">
			
				<h6>Well Options</h6>
				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link" data-toggle="modal" data-target="#modal-share" href="javascript:undefined">Share Well</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="explore.php">Go Back</a>
					</li>
				</ul>
			</aside>
			
			<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
				
				<?php
					
				// Connect to Database
				$db = db_connection();
				
				// Find all wishes belonging to the Unlisted list
				$result = $db->query( "SELECT COUNT(*) FROM ww_items WHERE user_id = $user_id AND list = 0" )->fetch();
				if ( $result[0] > 0 ) {
					echo '<h3>Unlisted</h3>';
					echo '<div class="card-deck">';
					
					// Display each wish
					$wishes = $db->query("SELECT * FROM ww_items WHERE user_id = $user_id AND list = 0" );
					foreach ( $wishes as $wish ) {
						echo new_wish( 'wish.php?id=' . $wish['id'], $wish['image'], $wish['title'], $wish['rating'] );	
					}
					
					echo '</div>';
				}
				
				// Find all of the lists that belong to that user
				$lists = $db->query("SELECT * FROM ww_items WHERE user_id = $user_id AND is_list = 1");
				foreach ( $lists as $list ) {
					
					echo '<h3>' . $list['title'] . '</h3>';
					echo '<p class="lead">' . $list['description'] . '</p>';
					echo '<div class="card-deck">';
					
					// Find all of the wishes that belong to this list
					$list_id = $list['id'];
					$wishes = $db->query("SELECT * FROM ww_items WHERE user_id = $user_id AND list = '$list_id'" );
					foreach ( $wishes as $wish ) {
						echo new_wish( 'wish.php?id=' . $wish['id'], $wish['image'], $wish['title'], $wish['rating'] );	
					}
					
					echo '</div>';
				}
				
				// If there are no lists or wishes
				if ( empty( $wishes ) ) {
					?>
					<div class="center no-wishes">
						<h1>Nothing to Show</h1>
						<p class="lead">This user doesn't have any wishes their account.</p>
					</div>
					<?php
				}
				
				$db = null;
				?>
				
			</main>
		</div>
	</div>
	<?php get_footer(); ?>
</body>
</html>