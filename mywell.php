<?php
/*
* mywell.php
* Your own personal well
*/

session_start();			
include 'config.php';
include 'functions.php';
?>

<!DOCTYPE html>
<html>
<?php get_meta(); ?>

<body>
	<?php
	// Get the correct user id from whomever is logged in
	$user_id = $_SESSION['user_id'];
		
	// Display a welcome message if it is your first time logging in
	$welcome = '
		<div class="center">
			<h1>Welcome</h1>
			<br>
			<p>Congratulations! You are now part of WishingWell.com. To get started, try making a wish by clicking "Make a Wish" on the sidebar.</p>
		</div>';
	build_modal( 'modal-welcome', 'Welcome', $welcome, 'close', null );
	if ( isset( $_POST['first-time-login'] ) and $_POST['first-time-login'] == true ) {
		?>
		<script>
			$(document).ready( function() {
				$('#modal-welcome').modal('show');
			});
		</script>
		<?php
		$_POST['first-time-login'] == false;
	}
	
	// Share wish modal
	share_modal( 'well', $user_id );
	?>
	
	<header id="header">
		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<a href="index.php"><img src="src/images/logo_flat.png" alt="logo"></a>
			</div>
			<div class="col-sm-8">
				<?php get_nav( $_GLOBAL['main_nav'], 'My Well' ); ?>
			</div>
		</div>
	</header>
	<div class="container-fluid">
		<div class="row">
			<aside id="sidebar" class="col-sm-3 col-md-2 d-none d-sm-block bg-light">
				<h6>Edit Well</h6>
				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link active" href="new-wish.php">Make a Wish</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="new-list.php">Make a List</a>
					</li>
				</ul>
			
				<h6>Well Options</h6>
				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link" data-toggle="modal" data-target="#modal-share" href="javascript:undefined">Share Well</a>
					</li>
				</ul>
			</aside>
			
			<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
				
				<?php
				
				// Connect to Database
				$db = db_connection();
				
				// Find all wishes belonging to the Unlisted list
				$result = $db->query( "SELECT COUNT(*) FROM ww_items WHERE user_id = $user_id AND list = 0")->fetch();
				if ( $result[0] > 0 ) {
					echo '<h3>Unlisted<a href="edit-list.php?id=' . $list['id'] . '"></h3>';
					echo '<div class="card-deck">';
					
					// Display each wish
					$wishes = $db->query("SELECT * FROM ww_items WHERE user_id = $user_id AND list = 0" );
					foreach ( $wishes as $wish ) {
						echo new_wish( 'edit-wish.php?id=' . $wish['id'], $wish['image'], $wish['title'], $wish['rating'] );	
					}
					
					echo '</div>';
				}
				
				// Find all of the lists that belong to that user
				$lists = $db->query("SELECT * FROM ww_items WHERE user_id = $user_id AND is_list = 1");
				foreach ( $lists as $list ) {
					
					echo '<h3>' . $list['title'] . '<a href="edit-list.php?id=' . $list['id'] . '"><span class="icon-pencil"></span></a></h3>';
					echo '<p class="lead">' . $list['description'] . '</p>';
					echo '<div class="card-deck">';
					
					// Find all of the wishes that belong to this list
					$list_id = $list['id'];
					$wishes = $db->query("SELECT * FROM ww_items WHERE user_id = $user_id AND list = $list_id" );
					foreach ( $wishes as $wish ) {
						echo new_wish( 'edit-wish.php?id=' . $wish['id'], $wish['image'], $wish['title'], $wish['rating'] );	
					}
					
					// Display a new wish card
					echo '<a href="new-wish.php?id=' . $list['id'] . '"><div class="card new-wish-card"></div></a>';
					echo '</div>';
				}
				
				$db = null;
				
				// If there are no lists or wishes
				if ( empty( $wishes ) ) {
					?>
					<div class="center no-wishes">
						<h1>Nothing to Show</h1>
						<p class="lead">You don't have any wishes. Click <a href="new-wish.php">here</a> to create one.</p>
					</div>
					<?php
				}
					
				?>
			</main>
		</div>
	</div>
	<?php get_footer(); ?>
</body>
</html>