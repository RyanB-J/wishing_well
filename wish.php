<?php
/*
* wish.php
* Information on someone else's wish
*/

session_start();
include 'config.php';
include 'functions.php';
?>

<!DOCTYPE html>
<html>
<?php get_meta(); ?>

<body>
	<?php
	include "login.php";
	
	// ID of the wish
	$wish_id = $_GET['id'];
	
	// Build Share modal
	share_modal( 'wish', $wish_id );
	
	// Get the wish information from the database
	$db = db_connection();
	$result = $db->query( "SELECT * FROM ww_items WHERE id = $wish_id" )->fetch();	
	?>
	<header id="header">
		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<a href="index.php"><img src="src/images/logo_flat.png" alt="logo"></a>
			</div>
			<div class="col-sm-8">
				<?php get_nav( $_GLOBAL['main_nav'], 'Explore' ); ?>
			</div>
		</div>
	</header>
	<div class="container-fluid">
		<div class="row">
			<aside id="sidebar" class="col-sm-3 col-md-2 d-none d-sm-block bg-light">
				<h6>Wish Options</h6>
				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link" data-toggle="modal" data-target="#modal-share" href="javascript:undefined">Share Wish</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="well.php?id=<?php echo $result['user_id']; ?>">Go Back</a>
					</li>
				</ul>
			</aside>
			
			<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
				<h3>Wish</h3>
				<div class="row">
					<div class="col-sm-6">
						<div class="center">
						<?php
						// Get image from database
						if ( ! empty( $result['image'] ) ) {
							echo '<img src="' . UPLOAD_PATH . $result['image'] . '" class="wish-img" alt="' . $result['title'] . '">';
						} else {
							echo '<img src="src/images/picture.png" class="wish-img" alt="picture">';
						}
						?>
						</div>
					</div>
					<div class="col-sm-6">
						<h4>Name</h4>
						<p><?php echo $result['title']; ?></p>
						<h4>Description</h4>
						<p><?php echo $result['description']; ?></p>
						<h4>Link</h4>
						<p><a href="<?php echo $result['link']; ?>" target="_blank"><?php echo $result['link']; ?></a></p>
						<h4>Rating</h4>
						<ul class="list-inline">
							<?php
								for ( $i = 0; $i < $result['rating']; $i++ ) {
									echo '<li class="list-inline-item star"><img src="src/star-on.svg" alt="star"></li>';
								}	
							?>
						</ul>
					</div>
				</div>
			</main>
		</div>
	</div>
	<?php 
	$db = null;
	get_footer();
	?>
</body>
</html>
